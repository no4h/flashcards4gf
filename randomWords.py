#!/usr/bin/env python3
import csv
from random import randint

listOfWords = []
lang = input("sup babe, you wanna translate From english or from german? \ntype en or ger, or haalp <3\n")
while (lang != "ger" and lang != "en" and lang != "haalp"):
    lang = input("babe, babe, you're doing it wrong, you can only type in en, ger or haalp\n")

HOWMANYNOUNS = 100
HOWMANYVERBS = 100
NUMWORDS = 100

wordsRight = 0
wordsTried = 0
currentWord = "initialize"
answer = "initialize"

mode = input("do u wanna practice verbs or nouns? or quit?\n")
fileString = ""
check = True

def getGerWord():
    return currentWordPair[0]

def getEnWord():
    return currentWordPair[1]

def findWord():
    return listOfWords[randint(1,NUMWORDS)]

def checkAnswer(answer, actualTranslation, currentWord):
    global wordsTried 
    wordsTried += 1
    if answer == actualTranslation:
        print("damn yer smart! now do another one \n")
        global wordsRight 
        wordsRight += 1
    else: 
        print("that ain't it babe, sorry.", currentWord, "is", actualTranslation, ".")
        print("")


def articleToLower(germWord):
    germWord = germWord.split(" ")
    germWord[0] = germWord[0].lower()
    return germWord[0] + " " + germWord[1]

def enToLower(enWord):
    return enWord.lower()


def nouns():
    global answer
    while (answer != "stahp"):
        # insert if lang == "haalp":
        if lang == "en":
            currentWordPair = findWord()
            currentWord = currentWordPair[0]
            translation = articleToLower(currentWordPair[1])
            print("translate", currentWord, "into German:")
            answer = input()
            checkAnswer(answer, translation, currentWord)
        if lang == "ger":
            currentWordPair = findWord()
            currentWord = articleToLower(currentWordPair[1])
            translation = (currentWordPair[0]).lower()
            print("translate", currentWord, "into English:")
            answer = input()
            checkAnswer(answer, translation, currentWord)
    print("okay bye bye bb <3")
    quit()

def verbs():
    global answer
    while (answer != "stahp"):
        # insert if lang == "haalp":
        if lang == "en":
            currentWordPair = findWord()
            currentWord = currentWordPair[0]
            translation = currentWordPair[-1]
            print("translate", currentWord, "into German:")
            answer = input()
            checkAnswer(answer, translation, currentWord)
        if lang == "ger":
            currentWordPair = findWord()
            currentWord = currentWordPair[-1]
            translation = currentWordPair[0]
            print("translate", currentWord, "into English:")
            answer = input()
            checkAnswer(answer, translation, currentWord)
    print("okay bye bye bb <3")
    quit()


while (check):
    
    if (mode == "quit"):
        print("okay bye bb <3")
        quit()
    
    if (mode == "nouns"):
        fileString = "germanNouns.csv"
        check = False
        NUMWORDS = HOWMANYNOUNS
        print("have some Nouns, and don't forget to capitalize Everything (not everything, but you know, the Nouns)")
        print()
    elif (mode == "verbs"):
        fileString = "germanVerbs.csv"
        check = False
        NUMWORDS = HOWMANYVERBS
        print("have some verbs, don't need to capitalize anything here.")
        if lang == "ger":
            print("you do need to add 'to' to every verb in english though")
        print()
    else: 
        print("yeah sorry", mode, "isn't really an option right now")
        mode = input("what is an option is verbs, nouns or quit")


with open(fileString, newline='') as csvfile:
	words = csv.reader(csvfile, delimiter=',', quotechar="\n")
	for row in words:
		listOfWords.append(row)
if mode == "nouns":
    nouns()
if mode == "verbs":
    verbs()


